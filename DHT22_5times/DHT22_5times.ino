#include <DHT22.h>
// Only used for sprintf
#include <stdio.h>

int PIN = 2;

// Setup a DHT22 instance
DHT22 myDHT[5] = { DHT22(2), DHT22(3), DHT22(4), DHT22(5), DHT22(6) };
DHT22 DHT224(4);

void setup(void)
{
  // start serial port
  Serial.begin(9600);
  Serial.println("DHT22 Library Demo");

  for( int i = PIN; i < PIN + 5; i++ ) {
    pinMode( i, INPUT );
  }
}

void loop(void)
{ 
  // The sensor can only be read from every 1-2s, and requires a minimum
  // 2s warm-up after power-on.
  delay(2000);

  float temp[5];
  float humid[5];

  float mtemp = 0;
  float mhumid = 0;

  float sigtemp=0;
  float sighumid=0;
  
  for( int dht = 0; dht < 5; dht ++ ) {
    // delay(500);
    DHT22_ERROR_t errorCode;

    errorCode = myDHT[ dht ].readData();
    switch(errorCode) {
      case DHT_ERROR_NONE:
        //char buf[128];
        /*sprintf(buf, "Integer-only reading %i: Temperature %hi.%01hi C, Humidity %i.%01i %% RH",
                     dht, 
                     myDHT[ dht ].getTemperatureCInt()/10, abs(myDHT[ dht ].getTemperatureCInt()%10),
                     myDHT[ dht ].getHumidityInt()/10 , myDHT[ dht ].getHumidityInt()%10 );
        Serial.println(buf);*/
        mtemp = mtemp + myDHT[ dht ].getTemperatureC();
        mhumid = mhumid + myDHT[ dht ].getHumidity();
        temp[ dht ] = myDHT[ dht ].getTemperatureC();
        humid[ dht ] = myDHT[ dht ].getHumidity();
    }
  }
  mtemp = mtemp / 5;
  mhumid = mhumid / 5;

  int imax = 0;
  float tmax = 0;
  for( int dht = 0; dht < 5; dht ++ ) {
    sigtemp += pow(temp[ dht ] - mtemp, 2);
    sighumid += pow( humid[ dht ] - mhumid, 2);
    if( abs( temp[ dht ] - mtemp ) > tmax ) {
      tmax = abs( temp[ dht ] - mtemp );
      imax = dht;
    }
  }
  sigtemp = sqrt( sigtemp / 4 );
  sighumid = sqrt( sighumid / 4 );
  
  Serial.print( mtemp );
  Serial.print( " +- ");
  Serial.print( sigtemp );
  Serial.print( " C " );
  Serial.print( imax );
  Serial.print( " " );
  Serial.println( tmax );
  Serial.print( mhumid );
  Serial.print( " +- ");
  Serial.print( sighumid );
  Serial.println( " % " );
}
